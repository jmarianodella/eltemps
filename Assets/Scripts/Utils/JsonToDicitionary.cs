using System;
using System.Collections;
using System.Collections.Generic;

namespace JMD.Utils
{
  class JsonToDictionary
  {

    public static string json;
    private static Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
    private static int counter;
    public static Dictionary<string, List<string>> Parse(string txt)
    {
      counter = 0;
      JsonToDictionary.json = txt.Trim();
      // quitamos llave inicial
      checkAndClearCharacter('{');
      // mientras tengamos elementos
      while (json[0] != '}')
      {
        if (json[0] == ',')
          checkAndClearCharacter(',');
        extractAttribute();
      }
      // quitamos llave final
      checkAndClearCharacter('}');


      return result;
    }

    private static void sliceJson()
    {
      json = json.Substring(1).Trim();
      counter++;
    }
    private static void extractAttribute()
    {
      string name;
      List<string> valueList = new List<string>();
      // extraemos nombre
      name = extractString();
      //quitamos los :
      checkAndClearCharacter(':');
      // extraemos la lista
      // puede venir ", {, [ o un número, nos limitamos a una lista: []
      valueList = extractList();

      result.Add(name, valueList);
    }

    private static string extractString()
    {
      string name = "";
      // quitamos primeras comillas
      checkAndClearCharacter('"');
      // recuperamos el nombre
      while (json[0] != '"')
      {
        name = name + json[0];
        json = json.Substring(1);
      }
      // quitamos ultimas comillas y los posibles espacios
      checkAndClearCharacter('"');

      return name;
    }

    private static List<string> extractList()
    {
      List<string> result = new List<string>();
      // quitamos corchete
      checkAndClearCharacter('[');
      while (json[0] != ']')
      {
        if (json[0] == ',')
          checkAndClearCharacter(',');
        result.Add(extractString());
      }
      checkAndClearCharacter(']');
      return result;
    }
    private static void checkAndClearCharacter(char c)
    {
      if (json[0] != c)
      {
        throw new Exception("Json mal formateado, no empieza por '" + c + "'");
      }
      json = json.Substring(1).Trim();
    }
  }

}