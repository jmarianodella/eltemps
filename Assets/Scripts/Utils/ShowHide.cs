﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHide : MonoBehaviour {

	// Use this for initialization
	public GameObject target;
	
	public void toggle(){
		target.SetActive(!target.activeSelf);
	}
}
