﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace JMD.Utils
{

  public class Draggable : MonoBehaviour,
  IDragHandler, IEndDragHandler, IBeginDragHandler, IPointerEnterHandler, IPointerExitHandler
  {
    Vector3 basePosition;
    Color colorHover = new Color(0.5f, 0.5f, 1, 0.2f);
    Color colorStay = new Color(0, 0, 0, 0);

    float transitionColorDuration = 0.2f;
    float currentTransmisionColorTime = 0;
    Color colorFrom, colorTo, currentColor;

    // parent
    Transform parent;
    GameObject fake = null;

    public bool draggable;
    public int siblingIndex;
    // Use this for initialization
    void Start()
    {
      basePosition = Vector3.positiveInfinity;
      colorFrom = colorStay;
      colorTo = colorStay;
      Transform tHover = gameObject.transform.Find("Hover");
      if (tHover != null)
      {
        currentColor = gameObject.transform.Find("Hover").GetComponent<Image>().color;
      }
      parent = transform.parent;
      siblingIndex = transform.GetSiblingIndex();
    }

    // Update is called once per frame
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
      if (currentTransmisionColorTime > 0)
      {
        currentTransmisionColorTime -= Time.deltaTime;
        currentColor = Color.Lerp(colorTo, colorFrom, currentTransmisionColorTime / transitionColorDuration);
        Transform hover = gameObject.transform.Find("Hover");
        Image image = hover.GetComponent<Image>();
        if (image){
          image.color = currentColor;
        }
      }
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
      if (!draggable) return;
      // basePosition = transform.position;
      parent = gameObject.transform.parent;
      gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

      siblingIndex = transform.GetSiblingIndex();
      fake = new GameObject();
      fake.transform.SetParent(parent);
      fake.transform.SetSiblingIndex(siblingIndex);

      gameObject.transform.SetParent(parent.transform.parent);

    }

    public void OnDrag(PointerEventData eventData)
    {
      if (!draggable) return;
      transform.position = Input.mousePosition;
      // Debug.Log(transform.position);
    }
    public void OnEndDrag(PointerEventData eventData)
    {
      if (!draggable) return;
      // transform.position = basePosition;

      gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
      transform.SetParent(parent);
      transform.SetSiblingIndex(siblingIndex);
      // Destroy(fake);

    }


    public void OnPointerEnter(PointerEventData eventData)
    {
      // asignamos colores de transición
      if (eventData.pointerDrag != null)
      {
        Debug.Log("in " + eventData.pointerDrag.name);
        Draggable buttonController = eventData.pointerDrag.GetComponent<Draggable>();
        int outerSibling = buttonController.siblingIndex;
        int newSiblingIndex = transform.GetSiblingIndex();
        buttonController.siblingIndex = newSiblingIndex;
        // transform.SetSiblingIndex(outerSibling);
      }
      transitionColor(colorHover);
      if (Input.GetMouseButtonDown(0))
      {
        print("arrosegnat botó " + eventData.pointerDrag.transform.GetSiblingIndex());

      }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
      transitionColor(colorStay);
    }
    void transitionColor(Color to)
    {
      colorTo = to;
      currentTransmisionColorTime = transitionColorDuration;

    }
  }

}