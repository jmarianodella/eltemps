﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace JMD.Utils
{
  [Serializable]
  public class EscenaInfo
  {
    public string[] tipus
    {
      get
      {
      string[] result = {};
       try
       {
           result = info["tipus"].ToArray();
       }
       catch (System.Exception)
       {
          Debug.LogWarning("no encuentra el campo tipus");
       }
        return result;
      }
    }
    // public string[] edicio;
    // public string[] actiu;
    // public string[] arxiu;
    // public string[] desenvolupament;
    private Dictionary<string, List<string>> info;

    private Config config; // publica sólo durante el desarrollo

    public void fromTextJson(string txt)
    {
      info = JsonToDictionary.Parse(txt);
    }

    public string[] get(string category)
    {
      return info[category].ToArray();
    }

    public void actualizar(string statusOld, string statusNew, string id)
    {
      // eliminamos el anterior registro
      List<string> oldList = info[statusOld];
      oldList.Remove(id);
      info[statusOld] = oldList;

      // actualizamos el nuevo
      List<string> newList = info[statusNew];
      if (statusNew == "actiu"){
        info["arxiu"].AddRange(newList);
        newList.Clear();
      }
      // para evitar posibles duplicados
      newList.Remove(id);
      newList.Add(id);
      info[statusNew] = newList;

      guardar();
    }

    private void guardar()
    {
      config = LoadConfig.Load();
      string sceneManagerPath = Application.streamingAssetsPath + "/" + config.escenes.sceneManager;

      if (File.Exists(sceneManagerPath))
      {
        File.Delete(sceneManagerPath);
      }
      using (StreamWriter file = File.CreateText(sceneManagerPath))
      {
        string output = "{";
        string txt = JsonUtility.ToJson(this);
        // output = output + this.info.ToString();
        bool mes1 = false; ;
        foreach (string key in this.info.Keys)
        {
          if (mes1)
          {
            output = output + " , ";
          }
          else
          {
            mes1 = true;
          }
          output = output + '"' + key + "\" : [";
          // add array itemps
          bool mes1element = false;
          foreach (string item in this.info[key])
          {
            if (mes1element)
            {
              output = output + " , ";
            }
            else
            {
              mes1element = true;
            }
            output += "\"" +  item + "\"";

          }
          output += "]";

        }
        //serialize object directly into file stream
        output = output + "}";
        file.Write(output);
      }
    }

    private string[] elimina(string[] list, string txt)
    {
      List<string> l = new List<string>(list);
      l.Remove(txt);
      return l.ToArray();
    }


    private string[] añade(string[] list, string txt)
    {
      List<string> l = new List<string>(list);
      // evitamos que existan duplicados
      l.Remove(txt);
      // l.Add(txt);
      l.Insert(0, txt);

      return l.ToArray();
    }
  }

}