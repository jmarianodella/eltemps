using System;
using System.Collections.Generic;

using JMD.Entity;

namespace JMD.Utils
{
  [Serializable]
  public class Config
  {
    public Login userValidation;
    public Escenes escenes;
    public Menu menu;
    public List<string> mapes;

    public Dictionary<string, Simbol> dSimbol = new Dictionary<string, Simbol>();

    public List<Simbol> simbols;

    public void initDSimbol()
    {
      foreach (Simbol item in simbols)
      {
        if (!dSimbol.ContainsKey(item.id))
        {
          dSimbol.Add(item.id, item);
        }
      }
    }

  }
}