using System;
using UnityEngine;

namespace JMD.Utils{
   [Serializable]
  public class Login {
    public string usuari;
    public string contrassenya;

    public bool validate (string u, string p){
      if (u == usuari){
        Debug.Log("usuari validat");
      }
      if (p == contrassenya){
        Debug.Log("contrassenya validat");
      }else {
        Debug.Log("m'han dit " + p + "; pero ès " + contrassenya);
      }
      return u == usuari && p == contrassenya;
    }
  }
}