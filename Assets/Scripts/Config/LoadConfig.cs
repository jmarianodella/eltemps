using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



namespace JMD.Utils{
   [Serializable]
  public class LoadConfig {

    static public Config Load(){
      string path;
      string jsonString;
      // Config c
      path = Application.streamingAssetsPath + "/config.json";
      jsonString = File.ReadAllText(path);
     return JsonUtility.FromJson<Config>(jsonString);
    }
  }
}