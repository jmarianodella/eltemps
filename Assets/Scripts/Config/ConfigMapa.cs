using System;

namespace JMD.Utils
{
  [Serializable]
  public class ConfigMapa
  {
    public string id;
    public string nom;
    public string thumbnail;
    public string objecte3D;
  }
}