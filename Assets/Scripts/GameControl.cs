﻿using JMD.Authenticate;
using JMD.Entity;
using JMD.Entity.Grafic;
using JMD.Utils;
using JMD.Menu;

using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// using 
namespace JMD.Game
{

  [RequireComponent(typeof(GrEscena))]
  public class GameControl : MonoBehaviour
  {
    private bool _modified;
    public bool modified
    {
      get { return _modified; }
      set
      {
        _modified = value;
        if (value)
        {
          buttons["Guardar"].SetActive(true);
        }
        // GameObject go = GameObject.Find("TopPanel");
        // if (go != null)
        // {
        //   MainMenuTopBar mmtb = go.GetComponent<MainMenuTopBar>();
        //   mmtb.asignaEscenaTop(_escena);
        // }
      }
    }
    public User user;
    public Escena _escena;
    public Escena escena
    {
      get { return _escena; }
      set
      {
        gameObject.GetComponent<GrEscena>().escena = value;
        _escena = value;
        GameObject go = GameObject.Find("TopPanel");
        if (go != null)
        {
          MainMenuTopBar mmtb = go.GetComponent<MainMenuTopBar>();
          mmtb.asignaEscenaTop(_escena);
        }
      }
    }
    // variables privadas
    private Dictionary<string, GameObject> buttons;
    public Config config; // publica sólo durante el desarrollo
                          // Use this for initialization
    public EscenaInfo escenaInfo;

    void Start()
    {

      DontDestroyOnLoad(gameObject);
      config = LoadConfig.Load();
      escenaInfo = null;
      buttons = new Dictionary<string, GameObject>();

    }

    // Update is called once per frame
    void Update()
    {
      hotKeys();
    }

    public Config GetConfig()
    {
      return this.config;
    }
    public void initButtonsMainMenu()
    {
      bindButtons();

      buttons["CargarEscena"].SetActive(false);
      buttons["DuplicarEscena"].SetActive(false);
      buttons["Guardar"].SetActive(false);
      buttons["TornarMainMenu"].SetActive(false);
    }

    public void initButtonsSceneEditor()
    {
      bindButtons();

      buttons["CargarEscena"].SetActive(false);
      buttons["DuplicarEscena"].SetActive(false);
      buttons["Guardar"].SetActive(true);
      buttons["TornarMainMenu"].SetActive(true);
    }
    public void asignaEscena(Escena escena)
    {
      this.escena = escena;
      buttons["CargarEscena"].SetActive(true);
      buttons["DuplicarEscena"].SetActive(true);
      // buttons["Guardar"].SetActive(true);
    }

    public void nuevaEscena()
    {
      this.escena = new Escena();
      this.escena.status = Escena.Estat.edicio;
      cargarEscenario("sceneEditor");
    }
    public void cargarEscena()
    {
      cargarEscenario("sceneEditor");
    }

    public void duplicarEscena()
    {
      escena.nuevoIdEscena();
    }

    public void guardaEscena()
    {
      // recuperamos el nombre
      string newName = GameObject.Find("EscenaName").GetComponent<InputField>().text;
      // recuperamos el status
      string newStatus = GameObject.Find("StatusEscena").GetComponent<JMDScrollController>().value;

      // si el estatus ha cambiado 
      // hay que eliminarlo del listado anterior 
      // y añadirlo al nuevo
      // bool estatusNuevo = (escena.estat != newStatus);

      // si el nombre recuperado no es blanco (no debería pasar)
      if (newName.Length != 0)
      {
        escena.nom = newName;
      }
      string path = Application.streamingAssetsPath + "\\" +config.escenes.path + "\\" + escena.id + ".json";
      if (File.Exists(path))
      {
        File.Delete(path);
      }
      using (StreamWriter file = File.CreateText(path))
      {
        string txt = JsonUtility.ToJson(escena);
        //serialize object directly into file stream
        file.Write(txt);
      }
      // if (estatusNuevo)
      // {
      escenaInfo.actualizar(escena.estat, newStatus, escena.id);
      escena.estat = newStatus;
      // actualizamos los botones
      // }
      GameObject.Find("InventoryMenu").GetComponent<LoadSceneList>().refreshButtons();

    }

    /**
		Carga los nombres de las escenas según su estatus
     */
    public EscenaInfo cargaListadoEscenas()
    {
      if (escenaInfo != null && escenaInfo.tipus.Length != 0)
      {
        return escenaInfo;
      }
      string sceneManagerPath = Application.streamingAssetsPath + "/" + config.escenes.sceneManager;
      string jsonString = File.ReadAllText(sceneManagerPath);

      // print (jsonString);
      // escenaInfo = JsonUtility.FromJson<EscenaInfo>(jsonString);
      escenaInfo = new EscenaInfo();
      escenaInfo.fromTextJson(jsonString);

      return escenaInfo;
    }


    public string[] listaDeEstadosDeEscenas()
    {
      return cargaListadoEscenas().tipus;
    }

    // carga la escena de edición de Escena
    public void cargarEscenario(string escenario)
    {
      SceneManager.LoadScene(escenario);
    }
    public void borrarMapaEscena(string mapaid)
    {
      List<Mapa> lmapa = new List<Mapa>(escena.mapa);
      foreach (Mapa item in lmapa)
      {
        if (item.id == mapaid)
        {
          lmapa.Remove(item);
          escena.mapa = lmapa.ToArray();
          return;
        }
      }
      escena.mapa = lmapa.ToArray();
    }

    public void asignaNuevoMapa(Mapa mapa)
    {
      GameObject.Find("InitSceneEditor").GetComponent<InitSceneEditor>().addMap(mapa);
      List<Mapa> lmapa = new List<Mapa>(escena.mapa);
      lmapa.Add(mapa);
      escena.mapa = lmapa.ToArray();
    }

    public void cargaBotonesNuevoMapaEscena(string container)
    {
      Config cnf = this.GetConfig();
      cnf.initDSimbol();
      Transform tContainer = GameObject.Find(container).transform;
      foreach (string idMap in config.mapes)
      {
        if (config.dSimbol.ContainsKey(idMap))
        {
          Simbol simbol = config.dSimbol[idMap];
          GameObject btn = Resources.Load<GameObject>("Menu/SceneEditor/NewMapButton");
          Mapa mapa = new Mapa();
          Simbol smapa = config.dSimbol[idMap];
          mapa.nom = smapa.nom;
          mapa.simbols = new Simbol[] { config.dSimbol[idMap] };
          btn.GetComponent<EscenaMapButtonController>().mapa = mapa;
          GameObject mapButton = Instantiate(btn);
          mapButton.transform.parent = tContainer;
          // btn
        }
        else
        {
          Debug.LogError("el símbols " + idMap + " no està definit");
        }
      }
      // ocultamos contenedor
      GameObject.Find("ContainerListMaps").SetActive(false);
      // GameObject.Find(container).SetActive(false);
    }
    private void bindButtons()
    {
      try
      {
        buttons.Clear();
      }
      catch (System.Exception)
      {
        buttons = new Dictionary<string, GameObject>();
      }
      buttons.Add("CargarEscena", GameObject.Find("CargarEscena"));
      buttons.Add("DuplicarEscena", GameObject.Find("DuplicarEscena"));
      buttons.Add("Guardar", GameObject.Find("Guardar"));
      buttons.Add("NovaEscena", GameObject.Find("NovaEscena"));
      buttons.Add("TornarMainMenu", GameObject.Find("TornarMainMenu"));
    }
    private void hotKeys()
    {
      // si hay un botón de control apretado
      bool controlIsPressed = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
      // duplicar escena
      if (Input.GetButton("Save") && controlIsPressed)
      {
        guardaEscena();
      }
      if ((Input.GetButton("Duplicate")) && controlIsPressed)
      {
        duplicarEscena();
        guardaEscena();
      }
      // if ((Input.GetKeyDown("z") ) && controlIsPressed)
      // {

      // }

    }

  }

}