﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JMD.Entity.Grafic
{

    public class GrMapa : MonoBehaviour
    {

        public Mapa mapa;
        public const float ESCALA = 10f;


        // Use this for initialization
        void Start()
        {

            // GameObject container = (GameObject)Resources.Load("Mapa");
            // goMap = Instantiate(container, Vector3.zero, Quaternion.identity);
            // goMap.SetActive(false);
            // goMap.name = mapa.id + "_" + mapa.nom;
            print("num simbols " + mapa.simbols.Length);
            
            foreach (Simbol item in mapa.simbols)
            {
                try
                {
                    GameObject s = loadSimbol(item);
                    if (s != null)
                    {
                        s.transform.parent = gameObject.transform;
                    }

                }
                catch (System.Exception e)
                {
                    Debug.Log(item.id);
                    Debug.LogException(e, this);
                }
            }

        }



        // Update is called once per frame
        void Update()
        {

        }

        // Carga la instancia d'un simbol
        GameObject loadSimbol(Simbol simbol)
        {
            Debug.Log(simbol.id);
            GameObject container = (GameObject)Resources.Load("Simbol");
            // GameObject container = (GameObject)Resources.Load("Models/" + simbol.id);
            if (container == null) return null;
            // posicion
            // simbol.px
            // simbol.pz
            Vector3 pos = new Vector3(float.Parse(simbol.px) , 0, -float.Parse(simbol.pz) );
            // rotacion
            // simbol.rotacio
            Quaternion rot = new Quaternion();
            rot = Quaternion.Euler(0, float.Parse(simbol.rotacio), 0);
            GameObject obj = Instantiate(container, pos * ESCALA, rot);
            obj.GetComponent<GrSimbol>().simbol = simbol;
            return obj;
        }

        public void enableMap(bool set)
        {
            gameObject.SetActive(set);
        }
    }

}