﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Entity;
using JMD.Utils;
public class PrimalSimbol : MonoBehaviour
{

  // Use this for initialization
  public List<Simbol> lsimbol;
  public Config config;

  public GameObject menu;

  void Start()
  {
    config = LoadConfig.Load();
  }

  // Update is called once per frame
  void Update()
  {
    setMenuOverPrimalSimbol();

  }

  void OnMouseDown()
  {
    Debug.Log("clicked");
    if (menu.activeSelf)
    {
      menu.SetActive(false);
    }
    else
    {
      menu.SetActive(true);
    }
  }

  void setMenuOverPrimalSimbol()
  {
    Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
    menu.transform.position = screenPos;
  }
}
