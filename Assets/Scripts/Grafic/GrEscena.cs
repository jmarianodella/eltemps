﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JMD.Entity.Grafic
{

  public class GrEscena : MonoBehaviour
  {
    public Escena escena;
    public int index;
    public List<string> lmapa;

    public bool playMode = false;

    // Use this for initialization
    void Start()
    {
      index = -1;
      lmapa = new List<string>();
    }

    // Update is called once per frame
    void Update()
    {
      if (playMode && Input.GetKeyDown(KeyCode.E))
      {
          Debug.Log(index);
          gameObject.transform.Find(lmapa[index]).gameObject.SetActive(false);
          index = (index + 1) % lmapa.Count;
          Debug.Log(index);
          gameObject.transform.Find(lmapa[index]).gameObject.SetActive(true);
          // GameObject.Find(lmapa[index]).SetActive(true);
      }
    }

    public void carregaMapes()
    {
      foreach (Mapa item in escena.mapa)
      {
        // GrMapa grmapa = gameObject.AddComponent<GrMapa>();
        // grmapa.mapa = item;
        lmapa.Add("m-" + item.id);
        GameObject m = carregaMapa(item);
        m.transform.parent = gameObject.transform;
        m.SetActive(false);
        index++;


      }
    }
    // Carga la instancia d'un mapa
    GameObject carregaMapa(Mapa mapa)
    {
      GameObject container = (GameObject)Resources.Load("Mapa");
      if (container == null) return null;
      // posicion
      // simbol.px
      // simbol.pz
      Vector3 pos = Vector3.zero;
      // rotacion
      // simbol.rotacio
      Quaternion rot = new Quaternion();
      rot = Quaternion.identity;
      GameObject obj = Instantiate(container, pos, rot);
      obj.name = "m-" + mapa.id;
      obj.GetComponent<GrMapa>().mapa = mapa;
      return obj;
    }

  }

}