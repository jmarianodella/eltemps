﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JMD.Entity.Grafic
{

    public class GrSimbol : MonoBehaviour
    {

        public Simbol simbol;
        public bool edit;
        // tipos de contenido 
        public enum eCONTINGUT  
        {
            símbol, texte, media, particles, pastilla
        }
        public eCONTINGUT[] lContingut;
        public enum eSIMBOL {
            sol, nubeBlanca, nubeNegra, ruta, chyron, txt, foto, mapa
        }

        public eSIMBOL tSimbol;

        // Use this for initialization
        void Start()
        {

            // GameObject container = (GameObject)Resources.Load("Mapa");
            // goMap = Instantiate(container, Vector3.zero, Quaternion.identity);
            // goMap.SetActive(false);
            // goMap.name = mapa.id + "_" + mapa.nom;
            print("Simbol " + simbol.id);
            try
            {
                GameObject s = loadSimbol(simbol);
                if (s != null)
                {
                    s.transform.parent = gameObject.transform;
                }

            }
            catch (System.Exception e)
            {
                Debug.Log(simbol.id);
                Debug.LogException(e, this);
            }

        }



        // Update is called once per frame
        void Update()
        {

        }

        // Carga la instancia d'un simbol
        GameObject loadSimbol(Simbol simbol)
        {
            Debug.Log(simbol.id);
            // GameObject container = (GameObject)Resources.Load("Models/bas1");
            GameObject container = (GameObject)Resources.Load("Models/" + simbol.id);
            if (container == null) {
                return null;
                // container = (GameObject)Resources.Load("Models/bas1");

            }
            // posicion
            Vector3 pos = new Vector3(float.Parse(simbol.px), 0, -float.Parse(simbol.pz));
            // rotacion
            Quaternion rot = new Quaternion();
            rot = Quaternion.Euler(0, float.Parse(simbol.rotacio), 0);
            GameObject obj = Instantiate(container, pos * 10f, rot);
            return obj;
        }

        public void enableMap(bool set)
        {
            gameObject.SetActive(set);
        }
    }

}