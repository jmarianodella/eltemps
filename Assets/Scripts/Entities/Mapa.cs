using System;

namespace JMD.Entity
{
    [Serializable]
    public class Mapa
    {
        public string id;
        public string nom;
        public string ordre;
        public string estat;
        public string trancisio;
        public string tnom;
        public string tduracio;

        public Simbol[] simbols;
    }
}