﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Reflection;
using UnityEngine;

namespace JMD.Entity
{
  [Serializable]
  public class Escena
  {
    public enum Estat
    {
      edicio, actiu, arxiu, meteo, desenvolupament, undefined
    }
    public string id;
    public string nom;
    public string data;
    public string autor;
    public Program programa;
    public Mapa[] mapa;

    public Estat status;

    public string estat
    {
      set
      {
        switch (value)
        {
          case "edicio":
            status = Estat.edicio;
            break;
          case "actiu":
            status = Estat.actiu;
            break;
          case "arxiu":
            status = Estat.arxiu;
            break;
          case "desenvolupament":
            status = Estat.desenvolupament;
            break;
          default:
            status = Estat.undefined;
            break;
        }
      }
      get
      {
        return status.ToString();
      }
    }
    public void nuevoIdEscena()
    {
      string id = "";
      System.Guid uuid = Guid.NewGuid();
      id = uuid.ToString();
      this.id = id;
    }


  }
}