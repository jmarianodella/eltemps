using System;

namespace JMD.Entity
{
    [Serializable]
    public class Simbol
    {
        public string id;
        public string nom;
        public TipusSimbol tipusSimbol;
        public string subtipus;
        public string textFormat;
        public string fonsFormat;
        public string text;
        public string px;
        public string pz;
        public string rotacio;
        public string imageId;
        public string cobertura;
        public string durada;
        public string transicio;
        public Objecte3D objecte3D;

    }
}