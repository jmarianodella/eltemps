﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using JMD.Utils;
using JMD.Game;


namespace JMD.Menu
{
  // Use this for initialization
  public class LoadMenuMapsButtons : MonoBehaviour
  {

    public Config cnf; // publica sólo durante el desarrollo
    public GameObject btn;
    // Use this for initialization
    void Start()
    {
      cnf = GameObject.Find("GameControl").GetComponent<GameControl>().GetConfig();
      Transform father = GameObject.Find("MapListContainer").transform;
      foreach (string id in cnf.mapes)
      {
        // preparamos el objeto
        // - textura
        string fileName = "Thumbnail/" + id;
        Texture2D texture = Resources.Load<Texture2D>(fileName);
        btn.GetComponent<RawImage>().texture = texture;
        // - texto
        // TMP_Text mapInfo = btn.transform.Find("Text").GetComponent<TMP_Text>();
        // mapInfo.SetText(mapsInfo);

        // generamos instancia
        GameObject newButton = Instantiate(btn);
        // lo ponemos en el container
        newButton.transform.parent = father;
        newButton.transform.SetAsFirstSibling();
      }
    }

    // Update is called once per frame
    void Update()
    {

    }
  }

}