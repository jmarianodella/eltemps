﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JMD.Entity;
using JMD.Game;

namespace JMD.Menu
{

  public class AddNewMap : MonoBehaviour
  {

    public Mapa mapa;
		GameControl game;
    // Use this for initialization
    void Start()
    {
      mapa = gameObject.GetComponent<EscenaMapButtonController>().mapa;
			game = GameObject.Find("GameControl").GetComponent<GameControl>();
    }
		public void addMap(){
			game.asignaNuevoMapa(mapa);
		}
  }

}