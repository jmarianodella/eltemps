﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

using JMD.Entity;
using JMD.Utils;
using JMD.Game;
public class LoadSceneList : MonoBehaviour
{

  // program configuration
  public string containerName;
  public string buttonName;
  public Config config;
  public EscenaInfo escenaInfo;
  public GameControl gameControl;
  GameObject button;
  GameObject buttonObj;

  // Use this for initialization
  void Start()
  {
    config = LoadConfig.Load();
    // botonera del menú top
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    // gameControl.initButtonsMainMenu();
    // contenedor de botons
    if (containerName.Length == 0)
    {
      containerName = config.menu.main.containerButtons;
    }

    // loadButtonsEscene();
  }
  public void loadButtonsEscene()
  {

    escenaInfo = gameControl.cargaListadoEscenas();

    if (buttonName.Length == 0)
    {
      buttonName = config.menu.main.button;
    }
    // print("button name " + buttonName);
    button = (GameObject)Resources.Load(buttonName);

    refreshButtons();

  }
  // Update is called once per frame
  void Update()
  {

  }

  public void refreshButtons()
  {
    clearButtons();
    createButtons();
  }

  private void createButtons()
  {
    foreach (string cat in escenaInfo.tipus)
    {
      foreach (var item in escenaInfo.get(cat))
      {
        instantiateButton(item, cat);
      }

    }
  }
  // crea un boton por cada escena
  // TODO: si el archivo asociado no existe eliminarlo de la lista y no generar botón
  private void instantiateButton(string item, string type)
  {
    // print(item);
    button.GetComponent<LoadScene>().filename = item;
    button.GetComponent<LoadScene>().status = type;
    buttonObj = Instantiate(button);


    // color segons el tipus de botó
    Color color = colorType(type);
    // print(containerName);
    buttonObj.transform.parent = GameObject.Find(containerName).transform;
    buttonObj.transform.SetAsFirstSibling();
    // buttonObj.transform.Find("SceneName").GetComponent<Text>().text = item;
    buttonObj.GetComponent<Image>().color = color;

  }

  private Color colorType(string type)
  {
    switch (type)
    {
      case "actiu":
        return new Color(0.8f, 1f, 0.8f, 1);
      // break;

      case "edicio":
        return new Color(0.8f, 0.8f, 1f, 1);
      // break;

      case "arxiu":
        return Color.white;
      // break;


      default:
        return new Color(0.8f, 0.8f, 0.8f, 1);
        // break;
    }
  }

  private void clearButtons()
  {
    GameObject buttonContainerOBJ = GameObject.Find(containerName);
    foreach (Transform item in buttonContainerOBJ.transform)
    {
      GameObject.Destroy(item.gameObject);
    }
  }

}
