﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using JMD.Entity;
using JMD.Entity.Grafic;
using JMD.Game;
using JMD.Utils;

public class LoadScene : MonoBehaviour {
  public Escena escena;
	public GameObject Manager;
	 // program configuration
	public Config config;

	public string filename;
	public string status;
	// Use this for initialization

	void Start () {
		  
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Awake(){
 		config = LoadConfig.Load();
		ReadScene();
		Manager = GameObject.Find("SceneInfo");
	}

	public void showInfoEscene(){
		GameObject sceneInfo = GameObject.Find("SceneInfo");
		
		// set scene name
		sceneInfo.transform.Find("Name").GetComponent<Text>().text = escena.nom;
		sceneInfo.transform.Find("Data").GetComponent<Text>().text = escena.data;
		string mapsInfo = "";
		foreach (Mapa item in escena.mapa)
		{
			string nomMapa = item.nom.Trim();
			if (nomMapa.Length == 0){
				nomMapa = "- no definit -";
			}
			mapsInfo += nomMapa + "\n"		;
		} 
		TMP_Text mapInfo = sceneInfo.transform.Find("Maps").GetComponent<TMP_Text>();
		mapInfo.SetText(mapsInfo);

		// pasamos información al gamecontrol
		GameObject gc = GameObject.Find("GameControl");
		if (gc != null){
			gc.GetComponent<GameControl>().asignaEscena(escena);
		}
	}
	private void ReadScene()
	{
		string path;
		string jsonString;

		// Debug.Log("Loading");
		
		path = Application.streamingAssetsPath + "/" + config.escenes.path + "/" + filename + ".json";
		// path = Application.streamingAssetsPath + "/scene.json";
		jsonString = File.ReadAllText(path);
		escena = JsonUtility.FromJson<Escena>(jsonString);
		escena.estat = status;
//		Debug.Log(escena.estat);

		gameObject.transform.Find("SceneName").GetComponent<Text>().text = escena.nom;
		gameObject.transform.Find("SceneData").GetComponent<Text>().text = escena.data;
		// EscenaInfo.transform.Find("Autor").GetComponent<Text>().text = escena.data;

		// GrEscena grE = Manager.GetComponent<GrEscena>();
		// grE.escena = escena;
		// grE.carregaMapes();

	}
}
