﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEngine.UI;

using JMD.Utils;
using JMD.Game;
using JMD.Entity;


namespace JMD.Menu
{
  // Use this for initialization
  public class LoadEscenaMapsButtons : MonoBehaviour
  {

    public Config cnf; // publica sólo durante el desarrollo
    public GameObject btn;
    // Use this for initialization
    void Start()
    {

    }

    public void loadMapes(Mapa[] lmapa)
    {
      cnf = GameObject.Find("GameControl").GetComponent<GameControl>().GetConfig();
      Transform father = GameObject.Find("EscenaMapesContainer").transform;
      foreach (Mapa mapa in lmapa)
      {
        // pasamos la información
        btn.GetComponent<EscenaMapButtonController>().mapa = mapa;

        // generamos instancia
        GameObject newButton = Instantiate(btn);

        // lo ponemos en el container
        newButton.transform.parent = father;
        newButton.transform.SetSiblingIndex(father.childCount - 2);
      }
    }

    public void loadMap(Mapa mapa)
    {
      Transform father = GameObject.Find("EscenaMapesContainer").transform;
      btn.GetComponent<EscenaMapButtonController>().mapa = mapa;

      // generamos instancia
      GameObject newButton = Instantiate(btn);

      // lo ponemos en el container
      newButton.transform.parent = father;
      newButton.transform.SetSiblingIndex(father.childCount - 2);
    }
    // Update is called once per frame
    void Update()
    {

    }
  }

}