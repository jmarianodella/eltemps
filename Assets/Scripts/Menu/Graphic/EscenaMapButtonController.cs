﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using JMD.Entity;

namespace JMD.Menu
{

  public class EscenaMapButtonController : MonoBehaviour
  {

    public Mapa _mapa;
    public Mapa mapa
    {
      set
      {
        _mapa = value;
        gameObject.name = value.nom;
        initButton();
      }
      get
      {
        return _mapa;
      }
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }
    void TaskOnClick()
    {
      //Output this to console when Button1 or Button3 is clicked
      Debug.Log("You have clicked the button controller!");
    }

    private void initButton()
    {

      // preparamos el objeto
      string fileName = "Thumbnail/" + mapa.simbols[0].id;

      // asignamos nombre al texto
      Text mapInfo = gameObject.transform.Find("Text").GetComponent<Text>();
      mapInfo.text = mapa.nom;
      // asignamos textura al objeto
      Texture2D texture = Resources.Load<Texture2D>(fileName);
      Transform raw = gameObject.transform.Find("RawImage");
      gameObject.transform.Find("RawImage").GetComponent<RawImage>().texture = texture;
      raw.SetAsFirstSibling();

      // asignamos el id al botón de borrar
      Transform deleteMap = gameObject.transform.Find("DeleteMap");
      if (deleteMap != null)
      {
        deleteMap.GetComponent<EscenaMapDeleteButton>().mapaid = mapa.id;
      }
    }





  }

}