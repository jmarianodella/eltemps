﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using JMD.Authenticate;
using JMD.Game;
using JMD.Entity;
using JMD.Menu;
public class MainMenuTopBar : MonoBehaviour
{

  // Use this for initialization
  public User user;
  public Escena escena;
  public GameObject userInfo;
  public GameControl gameControl;
  void Start()
  {
    printUser();
    try
    {
      initDesplegableStatus();

    }
    catch (System.Exception)
    {
    }

  }

  // Update is called once per frame
  void Update()
  {

  }

  public void logOut()
  {
    print("logging out");
    SceneManager.LoadScene("login");
  }

  public void newScene()
  {
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    gameControl.nuevaEscena();
  }
  public void duplicateScene()
  {
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    gameControl.duplicarEscena();
  }
  public void loadScene()
  {
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    gameControl.cargarEscena();
  }
  public void saveScene()
  {
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    gameControl.guardaEscena();
  }

  public void goMainMenu(){
      SceneManager.LoadScene("mainMenu");
  }
  // pinta el nombre de la escena en la barra de TOP
  public void asignaEscenaTop(Escena escena)
  {
    this.escena = escena;

    // asignamos el status de la escena
    JMDScrollController scrollController = GameObject.Find("StatusEscena").GetComponent<JMDScrollController>();
    scrollController.value = this.escena.estat;

    // asignamos el nombre de la escena al fieldInput
    GameObject sceneDropDown = GameObject.Find("EscenaName");
    sceneDropDown.GetComponent<InputField>().text = escena.nom;
  }

  public void escenaModificada()
  {
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    gameControl.modified = true;
  }
  // muestra el usuario validado
  void printUser()
  {
    // recuperamos el usuario validado
    GameObject gc = GameObject.Find("GameControl");

    if (gc != null)
    {
      // DEBUG
      user = gc.transform.GetComponent<GameControl>().user;
      Debug.Log("user: " + user.nom);
    }
    else
    {
      user = new User("testing", true);
    }
    // buscamos donde imprimir el nombre
    userInfo.GetComponent<Text>().text = user.nom;
  }

  // inicializa el desplegable de status
  public void initDesplegableStatus()
  {
    // recuperamos los status existentes
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    string[] lstatus = gameControl.listaDeEstadosDeEscenas();

    // buscamos el componente donde cargar el listado
    JMDScrollController scrollController = GameObject.Find("StatusEscena").GetComponent<JMDScrollController>();
    scrollController.initDropdown(lstatus);

  }

}
