﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JMD.Menu
{
  [RequireComponent(typeof(Dropdown))]
  public class JMDScrollController : MonoBehaviour
  {
    // llistat d'opcions
    string[] llistat;

    Dropdown ddown;
    // Use this for initialization
    void Start()
    {
      ddown = gameObject.GetComponent<Dropdown>();
      if (!ddown)
        Debug.LogError("No es troba el dropdown");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void initDropdown(string[] text)
    {
      llistat = text;
      if (ddown == null)
      {
        ddown = gameObject.GetComponent<Dropdown>();
      }
      ddown.ClearOptions();
      List<string> listOptions = new List<string>(text);
      ddown.AddOptions(listOptions);


    }

    public string value
    {
      set
      {
        int index = 0;
        foreach (var item in llistat)
        {
          if (item.ToLower() == value.ToLower())
          {
            ddown.value = index;
            return;
          }
          else
          {
            index++;
          }
        }
      }
      get
      {
        return llistat[ddown.value];
      }
    }

    public void next()
    {
      ddown.value = (ddown.value + 1) % llistat.Length;
    }
  }

}