﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Game;
using JMD.Menu;
using JMD.Entity;
public class InitSceneEditor : MonoBehaviour {

	private GameControl gameControl;
	private MainMenuTopBar mainMenuTopBar;

	private JMDScrollController jMDScroll;
	// Use this for initialization
	void Awake () {
		// inicializamos botones
		gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
		gameControl.initButtonsSceneEditor();
		gameControl.cargaBotonesNuevoMapaEscena("NewMapListContainer");
		// rellenamos barra top
		mainMenuTopBar = GameObject.Find("TopPanel").GetComponent<MainMenuTopBar>();
		mainMenuTopBar.initDesplegableStatus();
		mainMenuTopBar.asignaEscenaTop(gameControl.escena);
		
		mainMenuTopBar.user = gameControl.user;

		// rellenamos el listado de los mapas
		List<string> lmapas = new List<string>();
		foreach (Mapa mapa in gameControl.escena.mapa)
		{
				lmapas.Add(mapa.nom);
		}
		// jMDScroll = GameObject.Find("MapesLlistat").GetComponent<JMDScrollController>();
		// jMDScroll.initDropdown(lmapas.ToArray());
		LoadEscenaMapsButtons loadEscenaMapsButtons = GameObject.Find("EscenaMapesController").GetComponent<LoadEscenaMapsButtons>();
		loadEscenaMapsButtons.loadMapes(gameControl.escena.mapa);
	}
	
	public void addMap(Mapa mapa){
		LoadEscenaMapsButtons loadEscenaMapsButtons = GameObject.Find("EscenaMapesController").GetComponent<LoadEscenaMapsButtons>();
		loadEscenaMapsButtons.loadMap(mapa);
	}
	// Update is called once per frame
	void Update () {
		
	}
}
