﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Game;
using JMD.Menu;
public class InitMainMenu : MonoBehaviour
{

  private GameControl gameControl;
  private MainMenuTopBar mainMenuTopBar;

  // Use this for initialization
  void Start()
  {
    // inicializamos botones
    gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    gameControl.initButtonsMainMenu();

    // rellenamos barra top
    mainMenuTopBar = GameObject.Find("TopPanel").GetComponent<MainMenuTopBar>();
    mainMenuTopBar.initDesplegableStatus();

    // inicializamos los botones del inventario
    LoadSceneList loadSceneList = GameObject.Find("InventoryMenu").GetComponent<LoadSceneList>();
    loadSceneList.loadButtonsEscene();
  }

  // Update is called once per frame
  void Update()
  {

  }
}
