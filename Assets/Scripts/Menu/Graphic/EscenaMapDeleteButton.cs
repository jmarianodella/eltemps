﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JMD.Game;

namespace JMD.Menu
{

  public class EscenaMapDeleteButton : MonoBehaviour
  {
    public string mapaid;
    GameControl gameControl;

    // Use this for initialization
    void Start()
    {
      gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();

    }
    public void deleteMap()
    {
      gameControl.borrarMapaEscena(mapaid);
      Destroy(gameObject.transform.parent.gameObject);

    }
    // Update is called once per frame
    void Update()
    {

    }
    void TaskOnClick()
    {
      //Output this to console when Button1 or Button3 is clicked
      Debug.Log("You have clicked the button!");
    }

  }

}