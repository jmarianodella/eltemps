﻿using JMD.Entity;
using JMD.Entity.Grafic;
using JMD.Utils;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



namespace JMD.Menu
{


    public class MainMenu : MonoBehaviour
    {

        public Escena escena;
        public GameObject EscenaInfo;
        public GameObject Manager;
        public Canvas canvas;
        // program configuration
		public Config config;

        //para debug
        public FileInfo[] files;


        // Use this for initialization
        void Start()
        {
            config = LoadConfig.Load();
            llistaEscenesGuardades();
        }

        // Update is called once per frame
        void Update()
        {

        }

        // play games
        public void PlayGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        //quit program
        public void QuitGame()
        {
            Debug.Log("QUIT");
            //Application.Quit();
        }

        public void Load()
        {
            string path;
            string jsonString;

            Debug.Log("Loading");
            path = Application.streamingAssetsPath + "/scene.json";
            jsonString = File.ReadAllText(path);
            escena = JsonUtility.FromJson<Escena>(jsonString);
            Debug.Log(escena.nom);

            EscenaInfo.transform.Find("Nombre").GetComponent<Text>().text = escena.nom;
            EscenaInfo.transform.Find("Autor").GetComponent<Text>().text = escena.autor;

            GrEscena grE = Manager.GetComponent<GrEscena>();
            grE.escena = escena;
            grE.carregaMapes();

            canvas.enabled = false;

        }

        private void llistaEscenesGuardades(){
            //  Application.streamingAssetsPath + "/"
            string pathEscenes = Application.streamingAssetsPath + "/" + config.escenes.path;
            DirectoryInfo info = new DirectoryInfo(pathEscenes);
            files = info.GetFiles();

            foreach (FileInfo file in files) print (file);
        }
    }
}