﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using JMD.Utils;
using JMD.Authenticate;
using JMD.Game;

namespace JMD.Menu{



	public class Login : MonoBehaviour {

		// Use this for initialization
		private GameObject usuari;
		private GameObject password;
		private GameObject feedBack;

		// program configuration
		public Config config;



		void Start () {
			config = LoadConfig.Load();
			usuari = GameObject.Find("Usuari");
			password = GameObject.Find("Contrassenya");
			feedBack = GameObject.Find("FeedBack");
			// focus on usuari
			usuari.GetComponent<InputField>().Select();
			
		}
		
		// Update is called once per frame
		void Update () {
			if (Input.GetKeyDown(KeyCode.Tab)){
				canviaCampInput();
			}
			if (Input.GetKeyDown(KeyCode.Return)){
				validarUsuari();
			}
		}

		public void validarUsuari(){
			string usuariTxt = usuari.transform.Find("Text").GetComponent<Text>().text;
			InputField passwordIF = password.GetComponent<InputField>();
			// comprovam que estiguien els camps escrits
			string msg = "";
			if (usuariTxt == ""){
				msg += "no s'ha escrit l'usuari ";
			}
			if (passwordIF.text == ""){
				msg += "no s'ha indicat la contrassenya ";
			}
			if (msg != ""){
				mostraTexteFeedback(msg);
				return;
			}

			// validam els valors
			if (config.userValidation.validate(usuariTxt, passwordIF.text)){
				GameObject gc = GameObject.Find("GameControl");
				GameControl gameControl = gc.transform.GetComponent<GameControl>();
				gameControl.user = new User(usuariTxt, true);
				gameControl.cargarEscenario("mainMenu");
			} else {
				mostraTexteFeedback("Els usuaris no concorden");
			}
		}

		private void canviaCampInput(){
			InputField usuariIF = usuari.GetComponent<InputField>();
			InputField passwordIF = password.GetComponent<InputField>();
			if (usuariIF.isFocused) {
				passwordIF.Select();
			}else {
				usuariIF.Select();
			}

		}
		private void mostraTexteFeedback(string txt){
		feedBack.GetComponent<Text>().text = txt;
		}
}


}